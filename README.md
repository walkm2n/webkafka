# ![w](https://cdn4.iconfinder.com/data/icons/e-commerce-icon-set/48/Comment-20.png "Logo") WebKafka — Apache Kafka Web Client
WebKafka is the web interface for [Apache Kafka](http://kafka.apache.org/) messaging system. This software allows you to manage your topics and produce or consume messages via browser.

## Key features:
* Getting list of topics with offsets and last few messages
* Showing consumers subscribtions info
* Ability to write new messages to (non-)existing topics
* Working in cluster

![screenshot](http://i0.simplest-image-hosting.net/picture/webkafka.jpg) 

## Requirements
* _Apache Kafka_ 0.8.× or newer
* Installed _Java 8_
* _Apache Maven_
* Any modern web browser (it should support _IE8,_ but another browser preferably)

## Building and usage
Clone project to your computer:
```bash
git clone https://gitlab.com/walkm2n/webkafka.git
```
Before building the app you need to specify _Kafka_ and _ZooKeeper's_ settings in `application.properties` resource file. 
Then you need to build the project via [_Maven_](https://maven.apache.org/run-maven/#Quick_Start):
```bash
./mvn package
```
After you should move webkafka.jar in target directory to your path and run it via command like
```bash
java -jar webkafka.jar
```
Now you can open WebKafka in your browser. (Default port: 8352)