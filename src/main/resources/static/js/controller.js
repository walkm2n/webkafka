var kafkaClient = angular.module('webKafka', []).directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

kafkaClient.controller('Controller', function ($scope, $http) {
    $scope.messages = [];
    $http.get('/topics').
        success(function (data) {
            $scope.topics = data;
            $scope.sort = "name";
        });
    $http.get('/info').
        success(function (data) {
            $scope.info = data;
        });

    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {

        $('[data-toggle="popover"]').each(function () {
            $this = $(this);
            var text = $("#" + $this.data("link")).html();
            if ($this.data("count")) {
                $this.popover({
                    content: text,
                    placement: 'right',
                    html: true,
                    trigger: 'focus'
                });
            }
        });
    });
    $(document).on("click", ".linkTopicPartitions", function (e) {
        e.preventDefault();
        $this = $(this);
        if ($this.data("count"))
            $this.qtip({
                overwrite: false,
                content: $("#" + $this.data("link")).html(),
                show: {
                    event: e.type,
                    ready: true
                },
                position: {
                    my: 'top center',
                    at: 'bottom center'
                },
                style: {
                    classes: 'qtip-bootstrap'
                },
                hide: {
                    event: 'unfocus'
                }
            }, e);
    });

    function makeBeauty(what) {
        try {
            var input = eval('(' + what.text() + ')');
            what.jsonViewer(input);
        }
        catch (error) {
        }
    }

    $(document).on("mouseenter", ".message", function (e) {
        $(this).children(".message-key").children(".resend").show();
    });

    $(document).on("mouseleave", ".message", function (e) {
        $(this).children(".message-key").children(".resend").hide();
    });

    $(document).on("click", ".write", function (e) {
        e.preventDefault();
        $this = $(this);
        $.magnificPopup.open({
            items: {
                src: $('#modal'),
                type: 'inline'
            },
            callbacks: {
                beforeOpen: function () {
                    $("#sendmessagepartition").attr("value", "0");
                    var partitionshtml = "";
                    for (var i = 0; i < $this.data("partitions"); i++) {
                        partitionshtml = partitionshtml + "<li><a href='#'>" + i + "</a></li>";
                    }
                    $("#sendmessagepartitionlist").html(partitionshtml);
                    $("#displaypartition").text("Partition 0");

                    $topic = $("#sendmessagetopic");
                    if ($this.data("newmessageto") != "") {
                        $("#partition-choose").show();
                        $("#newMessageTo").text("New message to " + $this.data("newmessageto"));
                        $topic.val($this.data("newmessageto"));
                        $topic.addClass("form-control2");
                        $("#sendmessagegroup").removeClass("inp-group2");
                        $topic.prop("disabled", true);
                        if ($this.data("resend")) {
                            $("#displaypartition").text("Partition " + $this.data("newmessagetopart"));
                            $("#sendmessagepartition").attr("value", $this.data("newmessagetopart"));
                            $("#sendmessagetext").
                                text($("#Mes" + $this.data("newmessagetoindex")
                                + "_" + $this.data("newmessagetopart") + "_"
                                + $this.data("newmessagetooffset")).text().trim());
                            $("input[name='key']").attr("value", $this.data("newmessagetokey"));
                        }
                    } else {
                        $("#newMessageTo").text("Please, choose topic");
                        $("#partition-choose").hide();
                        $topic.val("");
                        $topic.removeClass("form-control2");
                        $("#sendmessagegroup").addClass("inp-group2");
                        $topic.prop("disabled", false);
                    }
                }
            }
        });
    });

    $("#sendmessagetopic").on("keyup paste change", function (event) {
        $(this).val($(this).val().replace(/[^a-z0-9-_]/gi, ''));

        if ($(this).val() != "") {
            $("#newMessageTo").text("New message to " + $(this).val());
        } else {
            $("#newMessageTo").text("Please, choose topic");
        }
    });

    $(document).on("click", "#sendmessagebutton", function (e) {
        e.preventDefault();
        $this = $(this);
        $form = $("#sendmessageform");
        $text = $("#sendmessagetext");
        $topic = $("#sendmessagetopic");
        $message = $("#sendmessagestatus");
        $message.text("").removeClass("red").removeClass("green");

        if ($topic.val() == "") {
            $message.addClass("red").text("Topic is empty");
            return;
        }
        if ($text.val() == "") {
            $message.addClass("red").text("Message is empty");
            return;
        }
        $("#sendmessagetopichidden").attr("value", $topic.val());
        $this.html("<i class='fa fa-circle-o-notch fa-spin'></i> Sending...");
        try {
            if ($("#sendmessagevalidate").is(':checked')) {
                console.log("Tryin to parse json");
                var input = eval("(" + $text.val() + ")");
                $("#jsonArea").jsonViewer(input);
            } else {
                console.log("Send message without json parsing");
            }
            $.ajax({
                url: '/send',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify($form.serializeObject())
            }).done(function () {
                $text.addClass('bg-success');
                $message.addClass("green").text("Message sent!");
                $this.html("<i class='fa fa-paper-plane'></i> Send another message");
            }).fail(function (msg, status) {
                console.log("msg:" + status);
                $text.addClass('bg-warning');
                $message.addClass("red").text("Server error.");
                $this.html("<i class='fa fa-paper-plane'></i> Try to send again");
            });
        }
        catch (error) {
            console.log(error);
            $message.addClass("red").text("JSON is not valid");
            $this.html("<i class='fa fa-paper-plane'></i> Try to send again");
        }
    });
    $(document).on("click", ".getMessages", function (e) {
        e.preventDefault();
        var $this = $(this);
        var $allbtns = $(".getMessages");
        var $link = $("div[link='" + $this.data("link") + "']");
        var $updateBtn = $("#update" + $this.data("topic_index") + "p" + $this.data("partition"));
        var getmes = "Get messages <span class='mdi-navigation-expand-more msgkey'></span>";
        var hidemes = "Hide messages <span class='mdi-navigation-expand-less msgkey'></span>";
        if ($this.data("count")) {
            if ($link.is(':visible')) {
                $link.slideUp();
                $updateBtn.fadeOut();
                $this.html(getmes);
            } else {
                $http.get('/topics/' + $this.data("topic") + "/" + $this.data("partition")).
                    success(function (data) {
                        if ($scope.messages[$this.data("topic_index")] == null) {
                            $scope.messages[$this.data("topic_index")] = [];
                        }
                        $scope.messages[$this.data("topic_index")][$this.data("partition")] = data;
                        $link.slideDown("fast", function () {
                            $('.toBeauty').each(function () {
                                makeBeauty($(this));
                            });
                        });
                    });
                $this.html(hidemes);
                $updateBtn.fadeIn();
            }
        }
    });

    $(document).on("click", ".updateMessages", function () {
        var $this = $(this);
        var $link = $("div[link='" + $this.data("link") + "']");
        var $updateBtn = $("#update" + $this.data("topic_index") + "p" + $this.data("partition"));
        $link.fadeOut("fast");
        $updateBtn.fadeOut();
        $http.get('/topics/' + $this.data("topic") + "/" + $this.data("partition")).
            success(function (data) {
                if ($scope.messages[$this.data("topic_index")] == null) {
                    $scope.messages[$this.data("topic_index")] = [];
                }
                $scope.messages[$this.data("topic_index")][$this.data("partition")] = data;
                $link.fadeIn("fast", function () {
                    $('.toBeauty').each(function () {
                        makeBeauty($(this));
                    });
                });

                $updateBtn.fadeIn();
            });
    });
    $(document).on("click", "#sendmessagepartitionlist li a", function () {
        $("#displaypartition").text("Partition " + $(this).text());
        $("#sendmessagepartition").attr("value", $(this).text());
    });
    $(document).on("focus", "#sendmessagetext", function () {
        $(this).removeClass("bg-success").removeClass("bg-warning");
    });
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});
