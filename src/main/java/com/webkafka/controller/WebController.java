package com.webkafka.controller;

import com.webkafka.domain.Message;
import com.webkafka.domain.SendMessage;
import com.webkafka.domain.Topic;
import com.webkafka.service.WebKafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class WebController {

    private WebKafkaService kafkaService;
    @Value("${kafka.brokers}")
    private String kafkaBrokers;
    @Value("${kafka.zookeeper.connect}")
    private String zookeeperConnect;
    @Value("${kafka.name}")
    private String kafkaName;

    @Autowired
    public WebController(WebKafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }

    @RequestMapping("/topics")
    @ResponseBody
    public List<Topic> getTopics() {
        return kafkaService.getTopics();
    }

    @RequestMapping(value = "/topics/{topic}/{partition}")
    @ResponseBody
    public Set<Message> getMessages(@PathVariable("topic") String topic, @PathVariable("partition") Integer partition) {
        return kafkaService.getMessages(topic, partition);
    }

    @RequestMapping(value = "/topics/{topic}")
    @ResponseBody
    public String getMessagesWithoutPartitions(@PathVariable("topic") String topic) {
        return "Please choose partition for " + topic + " topic.";
    }

    // This functionality isn't properly working yet
    @RequestMapping(value = "/delete/{topic}")
    @ResponseBody
    public String deleteTopic(@PathVariable("topic") String topic) {
        kafkaService.deleteTopic(topic);
        return "deleted";
    }

    @RequestMapping("/info")
    @ResponseBody
    public Map<String, String> getInfo() {
        Map<String, String> infoMap = new HashMap<>();
        infoMap.put("zookeeper", zookeeperConnect);
        infoMap.put("kafka", kafkaBrokers);
        infoMap.put("name", kafkaName);
        return infoMap;
    }

    @RequestMapping({"/"})
    public RedirectView showHomePage() {
        return new RedirectView("/index.html", true);
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    @ResponseBody
    public String add(@RequestBody SendMessage message) {
        kafkaService.sendMessage(message);
        return "{}";
    }
}
