package com.webkafka.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SendMessage {
    public final String topic;
    public final String key;
    public final String partition;
    public final String message;

    @JsonCreator
    public SendMessage(
            @JsonProperty("topic") String topic,
            @JsonProperty("key") String key,
            @JsonProperty("partition") String partition,
            @JsonProperty("message") String message) {
        this.topic = topic;
        this.key = key;
        this.partition = partition;
        this.message = message;
    }
}
