package com.webkafka.domain;

import java.util.HashSet;
import java.util.Set;

public class Topic {
    public String name;
    public Set<Partition> partitions = new HashSet<>();
    public boolean active;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Topic topic = (Topic) o;

        if (name != null ? !name.equals(topic.name) : topic.name != null) return false;
        if (partitions != null ? !partitions.equals(topic.partitions) : topic.partitions != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (partitions != null ? partitions.hashCode() : 0);
        return result;
    }
}
