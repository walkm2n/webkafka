package com.webkafka.domain;

import java.util.ArrayList;
import java.util.List;

public class Partition {

    public int name;
    public long lastOffset;
    public boolean haveMessage;
    public List<Consumer> consumers = new ArrayList<>();

    public void addConsumer(String name, Long offset) {
        Consumer consumer = new Consumer();
        consumer.name = name;
        consumer.offset = offset;
        consumers.add(consumer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Partition partition = (Partition) o;

        if (lastOffset != partition.lastOffset) return false;
        if (name != partition.name) return false;
        if (consumers != null ? !consumers.equals(partition.consumers) : partition.consumers != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name;
        result = 31 * result + (int) (lastOffset ^ (lastOffset >>> 32));
        result = 31 * result + (consumers != null ? consumers.hashCode() : 0);
        return result;
    }
}
