package com.webkafka.domain;

public class Consumer {
    public String name;
    public Long offset;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Consumer consumer = (Consumer) o;

        if (name != null ? !name.equals(consumer.name) : consumer.name != null) return false;
        return !(offset != null ? !offset.equals(consumer.offset) : consumer.offset != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (offset != null ? offset.hashCode() : 0);
        return result;
    }
}
