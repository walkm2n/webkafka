package com.webkafka.domain;

public class Message {
    public String key;
    public String message;
    public long offset;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message1 = (Message) o;

        if (offset != message1.offset) return false;
        if (key != null ? !key.equals(message1.key) : message1.key != null) return false;
        return !(message != null ? !message.equals(message1.message) : message1.message != null);

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (int) (offset ^ (offset >>> 32));
        return result;
    }
}
