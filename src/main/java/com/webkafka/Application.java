package com.webkafka;


import kafka.javaapi.consumer.SimpleConsumer;
import org.I0Itec.zkclient.ZkClient;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;
import java.util.List;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableWebMvc
public class Application {
    private final static Logger log = LoggerFactory.getLogger(Application.class);
    @Value("${kafka.client.id}")
    private String clientId;
    @Value("${kafka.producer.buffer.size}")
    private int producerBufferSize;
    @Value("${kafka.connection.timeout}")
    private int connectionTimeOut;
    @Value("${kafka.zookeeper.connect}")
    private String zookeeperConnect;
    @Value("#{'${kafka.brokers}'.split(',')}")
    private List<String> kafkaBrokers;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setShowBanner(false);
        app.run(args);
    }

    @Bean
    public SimpleConsumer simpleConsumer() {
        String[] urlAndPort = kafkaBrokers.get(0).split(":");
        return new SimpleConsumer(
                urlAndPort[0],
                Integer.parseInt(urlAndPort[1]),
                connectionTimeOut,
                producerBufferSize,
                clientId);
    }

    @Bean
    public ZooKeeper zooKeeper() throws IOException {
        return new ZooKeeper(zookeeperConnect, connectionTimeOut, event -> {
            Watcher.Event.KeeperState state = event.getState();
            if (state == Watcher.Event.KeeperState.Expired) {
                log.info("Zookeeper session has [{}] server process shutting down.", state);
            }
        }
        );

    }

    @Bean
    public ZkClient zkClient() {
        return new ZkClient(zookeeperConnect, connectionTimeOut);
    }
}
