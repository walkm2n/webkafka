package com.webkafka.service;

import com.webkafka.dao.KafkaDao;
import com.webkafka.dao.ZooKeeperDao;
import com.webkafka.domain.Message;
import com.webkafka.domain.SendMessage;
import com.webkafka.domain.Topic;
import com.webkafka.logic.TopicsLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class WebKafkaService {
    @Autowired
    private KafkaDao kafkaDao;
    @Autowired
    private ZooKeeperDao zooKeeperDao;

    @Value("${web.number.of.messages}")
    private int numberOfMessages;

    public List<Topic> getTopics() {
        TopicsLogic topicsLogic = new TopicsLogic(kafkaDao, zooKeeperDao);
        return topicsLogic.getTopics();
    }

    public void deleteTopic(String topic) {
        zooKeeperDao.deleteTopic(topic);
    }

    public Set<Message> getMessages(String topic, Integer partition) {
        return kafkaDao.getMessages(topic, partition, numberOfMessages);
    }

    public void sendMessage(SendMessage message) {
        try {
            kafkaDao.sendMessage(
                    message.topic,
                    message.key,
                    message.partition,
                    message.message
            );
        } catch (Exception e) {
            kafkaDao.sendMessage(
                    message.topic,
                    message.key,
                    message.partition,
                    message.message
            );
        }
    }
}