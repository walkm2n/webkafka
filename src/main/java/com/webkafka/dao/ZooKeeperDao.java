package com.webkafka.dao;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class ZooKeeperDao {
    private final static Logger log = LoggerFactory.getLogger(ZooKeeperDao.class);
    @Autowired
    ZooKeeper zooKeeper;

    public List<String> getListOfConsumers() {
        try {
            return zooKeeper.getChildren("/consumers", null);
        } catch (KeeperException | InterruptedException e) {
            log.error("Cant get list of consumers.", e);
            return Collections.emptyList();
        }
    }

    public List<String> getListOfTopics(String groupId) {
        String topicsPath = "/consumers/" + groupId + "/offsets";

        try {
            if (zooKeeper.exists(topicsPath, null) != null) {
                return zooKeeper.getChildren(topicsPath, null);
            }
        } catch (KeeperException | InterruptedException e) {
            log.error("Cant get list of consumers.", e);
        }

        return Collections.emptyList();
    }

    public List<String> getListOfPartition(String groupId, String topic) {
        String partitionsPath = "/consumers/" + groupId + "/offsets/" + topic;

        try {
            if (zooKeeper.exists(partitionsPath, null) != null) {
                return zooKeeper.getChildren(partitionsPath, null);
            }
        } catch (KeeperException | InterruptedException e) {
            log.error("Cant get list of partitions.", e);
        }

        return Collections.emptyList();
    }

    public Optional<Long> getOffsetOfConsumer(String groupId, String topic, Integer partition) {
        try {
            byte[] s = zooKeeper.getData("/consumers/" + groupId + "/offsets/" + topic + "/" + partition, false, null);
            return Optional.of(Long.parseLong(new String(s)));
        } catch (KeeperException | InterruptedException e) {
            log.error("Cant get list of partitions.", e);
        }
        return Optional.empty();
    }

    public void deleteTopic(String topic) {
        log.info("Try to delete topic " + topic);
        try {
            createPath("/admin/delete_topics/" + topic);
        } catch (Exception e) {
            log.error("Error", e);
        }
    }

    public void createPath(String path) throws InterruptedException, KeeperException {
        if (zooKeeper.exists(path, null) == null && path.length() > 0) {
            String temp = path.substring(0, path.lastIndexOf("/"));
            createPath(temp);
            zooKeeper.create(path, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        }
    }

    public boolean isTopicExists(String topic) {
        try {
            if (zooKeeper.exists("/brokers/topics/" + topic, null) != null) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    private boolean deletePath(String path) {
        List<String> children;
        try {
            children = zooKeeper.getChildren(path, null);
        } catch (KeeperException | InterruptedException e) {
            return true;
        }

        for (String child : children) {
            if (!deletePath(path + "/" + child)) {
                return false;
            }
        }

        try {
            zooKeeper.delete(path, -1);
            return true;
        } catch (KeeperException | InterruptedException e) {
            return false;
        }
    }
}
