package com.webkafka.dao;

import com.webkafka.domain.Message;
import com.webkafka.domain.Partition;
import com.webkafka.domain.Topic;
import kafka.api.FetchRequest;
import kafka.api.FetchRequestBuilder;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.common.TopicAndPartition;
import kafka.javaapi.*;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.javaapi.message.ByteBufferMessageSet;
import kafka.javaapi.producer.Producer;
import kafka.message.MessageAndOffset;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import scala.collection.JavaConversions;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.function.Supplier;

@Repository
public class KafkaDao {
    private final static Logger log = LoggerFactory.getLogger(KafkaDao.class);
    private final Properties properties = new Properties();
    @Value("#{'${kafka.brokers}'.split(',')}")
    private List<String> kafkaBrokers;
    @Value("${kafka.brokers}")
    private String kafkaBrokersString;
    @Autowired
    SimpleConsumer consumer;
    @Autowired
    ZkClient zkClient;

    @Value("${kafka.host}")
    private String kafkaHost;
    @Value("${kafka.client.id}")
    private String clientId;
    @Value("${kafka.port}")
    private int kafkaPort;
    @Value("${kafka.connection.timeout}")
    private int connectionTimeOut;
    @Value("${kafka.producer.buffer.size}")
    private int producerBufferSize;
    @Value("${kafka.zookeeper.connect}")
    private String zookeeperConnect;

    public List<Topic> getTopics() {
        Set<TopicAndPartition> topicsAndPartitions = JavaConversions.asJavaSet(ZkUtils.getAllPartitions(zkClient));
        Map<String, Set<Integer>> topicMap = getTopicMap(topicsAndPartitions);
        Map<String, Map<Integer, Long>> topicMapWithOffsets = getOffsetMap(topicMap, getLastOffsets(topicsAndPartitions));

        List<Topic> topics = new ArrayList<>();
        Supplier<Topic> topicSupplier = Topic::new;
        for (Map.Entry<String, Map<Integer, Long>> t : topicMapWithOffsets.entrySet()) {
            Topic topic = topicSupplier.get();
            topic.name = t.getKey();
            topic.partitions = findActivePartitions(t);
            topic.active = topic.partitions.stream().anyMatch((p) -> p.haveMessage);
            topics.add(topic);
        }

        return topics;
    }

    private Set<Partition> findActivePartitions(Map.Entry<String, Map<Integer, Long>> topicWithPartitions) {
        Set<Partition> partitions = new HashSet<>();

        for (Map.Entry<Integer, Long> p : topicWithPartitions.getValue().entrySet()) {
            Partition partition = new Partition();
            partition.name = p.getKey();
            partition.lastOffset = p.getValue();
            partition.haveMessage = isPartitionActive(topicWithPartitions.getKey(), p.getKey(), p.getValue());

            partitions.add(partition);
        }

        return partitions;
    }

    private Map<String, Map<Integer, Long>> getOffsetMap(Map<String, Set<Integer>> topicMap, OffsetResponse offsets) {
        Map<String, Map<Integer, Long>> result = new HashMap<>();
        for (Map.Entry<String, Set<Integer>> topic : topicMap.entrySet()) {
            Map<Integer, Long> partitionToOffset = new HashMap<>();
            topic.getValue().stream().forEach(
                    p -> partitionToOffset.put(p, offsets == null ? 0 : offsets.offsets(topic.getKey(), p).length > 0 ? offsets.offsets(topic.getKey(), p)[0] : 0));

            result.put(topic.getKey(), partitionToOffset);
        }

        return result;
    }

    private Map<String, Set<Integer>> getTopicMap(Set<TopicAndPartition> topicsAndPartitions) {
        Map<String, Set<Integer>> topicMap = new HashMap<>();
        topicsAndPartitions.forEach(t -> topicMap.compute(t.topic(),
                (k, v) -> {
                    if (v == null) {
                        return Collections.singleton(t.partition());
                    } else {
                        Set<Integer> temp = new HashSet<>();
                        temp.addAll(v);
                        temp.add(t.partition());
                        return temp;
                    }
                }));
        return topicMap;
    }

    public OffsetResponse getLastOffsets(Set<TopicAndPartition> topicAndPartitions) {
        Optional<TopicAndPartition> first = topicAndPartitions.stream().findFirst();
        if (first.isPresent()) findLeader(first.get().topic(), first.get().partition());

        PartitionOffsetRequestInfo partitionOffsetRequestInfo = new PartitionOffsetRequestInfo(-1, 1);
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo = new HashMap<>();

        topicAndPartitions.stream().forEach(t -> requestInfo.put(t, partitionOffsetRequestInfo));

        return getOffsetResponse(requestInfo);
    }

    public Long getLastOffset(String topic, Integer partition) {
        findLeader(topic, partition);
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo =
                Collections.singletonMap(
                        new TopicAndPartition(topic, partition), new PartitionOffsetRequestInfo(-1, 1));

        OffsetResponse response = getOffsetResponse(requestInfo);
        if (response == null) return 0L;

        return response.offsets(topic, partition)[0];
    }

    private OffsetResponse getOffsetResponse(Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo) {
        OffsetRequest request = new OffsetRequest(
                requestInfo, kafka.api.OffsetRequest.CurrentVersion(), clientId);

        OffsetResponse response = consumer.getOffsetsBefore(request);
        if (response.hasError()) {
            log.error("Error fetching data Offset Data the Broker.");
            return response;
        }
        return response;
    }

    public boolean isPartitionActive(String topic, Integer partition, Long lastOffset) {
        return !getMessages(topic, partition, 1, lastOffset).isEmpty();
    }

    public Set<Message> getMessages(String topic, Integer partition, Integer reads) {
        return getMessages(topic, partition, reads, getLastOffset(topic, partition));
    }

    public Set<Message> getMessages(String topic, Integer partition, Integer reads, Long lastOffset) {
        findLeader(topic, partition);
        Long startOffset = lastOffset - reads;

        if (startOffset < 0) startOffset = 0L;
        log.info("Get {} messages from topic: {}({}) (Last offset: {})", reads, topic, partition, lastOffset);
        Set<Message> messages = new HashSet<>();
        while (!startOffset.equals(lastOffset)) {
            log.info("Try to read topic {} starting from {} offset. Last offset of this part: {}",
                    topic, startOffset, lastOffset);
            FetchRequest request = new FetchRequestBuilder()
                    .clientId(clientId)
                    .addFetch(topic, partition, startOffset, producerBufferSize)
                    .maxWait(connectionTimeOut)
                    .build();
            FetchResponse response = consumer.fetch(request);
            startOffset++;
            messages.addAll(getListOfMessages(response.messageSet(topic, partition)));
        }

        return messages;
    }

    private Set<Message> getListOfMessages(ByteBufferMessageSet messageSet) {
        Set<Message> listAllMessages = new HashSet<>();
        messageSet.forEach(m -> listAllMessages.add(createMessage(m)));

        return listAllMessages;
    }

    private Message createMessage(MessageAndOffset messageAndOffset) {
        Message message = new Message();
        message.offset = messageAndOffset.offset();
        message.message = getStringFromPayload(messageAndOffset.message().payload());
        if (messageAndOffset.message().hasKey()) {
            message.key = getStringFromPayload(messageAndOffset.message().key());
        }

        return message;
    }

    private String getStringFromPayload(ByteBuffer payload) {
        try {
            byte[] bytes = new byte[payload.limit()];
            payload.get(bytes);

            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("Something wrong with encoding.");
        }
        return null;
    }

    public void sendMessage(String topic, String key, String partition, String message) {
        log.info("Send message to {}({}). key = {}, message = {}", topic, partition, key, message);
        properties.put("metadata.broker.list", kafkaBrokersString);
        properties.put("serializer.class", "kafka.serializer.StringEncoder");

        ProducerConfig config = new ProducerConfig(properties);
        Producer<String, String> producer = new Producer<>(config);
        KeyedMessage<String, String> data =
                new KeyedMessage<>(topic, key, partition, message.replaceAll("(\\r|\\n|\\t)", ""));
        producer.send(data);
        producer.close();
    }

    private PartitionMetadata findLeader(String topic, int partition) {
        PartitionMetadata returnMetaData = null;
        loop:
        for (String broker : kafkaBrokers) {
            SimpleConsumer consumer = null;
            try {
                String[] urlAndPort = broker.split(":");
                consumer = new SimpleConsumer(
                        urlAndPort[0],
                        Integer.parseInt(urlAndPort[1]),
                        connectionTimeOut,
                        producerBufferSize,
                        clientId);

                TopicMetadataRequest req = new TopicMetadataRequest(Collections.singletonList(topic));
                List<TopicMetadata> metaData = consumer.send(req).topicsMetadata();

                for (TopicMetadata item : metaData) {
                    for (PartitionMetadata part : item.partitionsMetadata()) {
                        if (part.partitionId() == partition) {
                            returnMetaData = part;
                            break loop;
                        }
                    }
                }
            } catch (Exception e) {
                log.error("Error communicating with Broker {} to find Leader for {} ({})",
                        broker, topic, partition, e);
            } finally {
                this.consumer = consumer;
            }
        }

        return returnMetaData;
    }
}
