package com.webkafka.logic;

import com.webkafka.dao.KafkaDao;
import com.webkafka.dao.ZooKeeperDao;
import com.webkafka.domain.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TopicsLogic {
    private final static Logger log = LoggerFactory.getLogger(TopicsLogic.class);
    private KafkaDao kafkaDao;
    private ZooKeeperDao zooKeeperDao;
    private List<Topic> topics = new ArrayList<>();

    public TopicsLogic(KafkaDao kafkaDao, ZooKeeperDao zooKeeperDao) {
        this.kafkaDao = kafkaDao;
        this.zooKeeperDao = zooKeeperDao;
    }

    public List<Topic> getTopics() {
        topics = kafkaDao.getTopics();
        attachConsumersToTopics();
        return topics;
    }

    private List<Topic> attachConsumersToTopics() {
        zooKeeperDao.getListOfConsumers().forEach((consumer) -> {
            zooKeeperDao.getListOfTopics(consumer).forEach((topic) -> {
                zooKeeperDao.getListOfPartition(consumer, topic).stream().map(Integer::valueOf).forEach((partition) -> {
                    Optional<Long> offset = zooKeeperDao.getOffsetOfConsumer(consumer, topic, partition);
                    if (offset.isPresent()) addConsumerToTopic(consumer, topic, partition, offset.get());
                });
            });
        });

        return topics;
    }

    private void addConsumerToTopic(final String consumer, final String topicName, final Integer partitionName, final Long offset) {
        topics.stream().filter((t) -> t.name.equals(topicName)).forEach((topic) -> {
            topic.partitions.stream().filter((p) -> p.name == partitionName).forEach((partition -> {
                partition.addConsumer(consumer, offset);
                log.trace("Found consumer: {} read {}({}) with offset {}", consumer, topicName, partition.name, offset);
            }));
        });
    }
}
